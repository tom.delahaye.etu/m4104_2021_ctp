package fr.ulille.iutinfo.teletp;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class SuiviViewModel extends AndroidViewModel {
    //Class Attributes
    private MutableLiveData<String> liveLocalisation;
    private MutableLiveData<String> liveUsername;
    // TODO Q2.a
    String[] questions;
    MutableLiveData<Integer> liveNextQuestion;

    //Constructor
    public SuiviViewModel(Application application) {
        super(application);
        liveLocalisation = new MutableLiveData<>();
        liveUsername = new MutableLiveData<>();
        liveNextQuestion = new MutableLiveData<>();
        liveNextQuestion.setValue(0);
    }

    //Methods
    //Getters
    public LiveData<String> getLiveUsername() {
        return liveUsername;
    }
    public void setUsername(String username) {
        liveUsername.setValue(username);
        Context context = getApplication().getApplicationContext();
        Toast toast = Toast.makeText(context, "Username : " + username, Toast.LENGTH_LONG);
        toast.show();
    }
    public String getUsername() {
        return liveUsername.getValue();
    }
    public LiveData<String> getLiveLocalisation() {
        return liveLocalisation;
    }
    //Setters
    public void setLocalisation(String localisation) {
        liveLocalisation.setValue(localisation);
        Context context = getApplication().getApplicationContext();
        Toast toast = Toast.makeText(context, "Localisation : " + localisation, Toast.LENGTH_LONG);
        toast.show();
    }
    public String getLocalisation() {
        return liveLocalisation.getValue();
    }
    
    // TODO Q2.a

    void initQuestions(Context context){
        questions = new MutableLiveData<>(context.getResources().getStringArray(R.array.list_questions));
    }
    String getQuestions(int position){
        if(position>0 && position<questions.length-1){
            return questions[position];
        }else{
            return questions[0];
        }
    }
    //LiveData<Integer> getLiveNextQuestion(){ return ; }
    //void setNextQuestion(Integer nextQuestion){}
    //Integer getNextQuestion(){}

}
